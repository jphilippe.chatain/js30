const player = document.querySelector(".player");
const video = player.querySelector(".viewer");
const progress = player.querySelector(".progress");
const progressBar = player.querySelector(".progress__filled");
const toggle = player.querySelector(".toggle");
const skipButtons = player.querySelectorAll("[data-skip]");
const ranges = player.querySelectorAll(".player__slider");
const fullscreenButton = player.querySelector(".fullscreen__button");

function togglePlay() {
    if (video.paused) {
        video.play();
    } else {
        video.pause();
    }
}

function updateButton() {
    const icon = this.paused ? "►" : "▌▌";
    toggle.textContent = icon;
}

function onTimeUpdate() {
    progressBar.style.flexBasis = `${video.currentTime / video.duration * 100}%`;
}

function skip(skipDuration) {
    video.currentTime += skipDuration;
}

function onRangeChange(e) {
    video[e.target.name] = parseFloat(e.target.value);
}

function handleSeeking(e) {
    setVideoTime(e.offsetX / progress.offsetWidth);
}

function setVideoTime(percentage) {
    video.currentTime = percentage * video.duration;
}

function handleFullscreen() {
    if (!document.fullscreenElement) {
        video.requestFullscreen();
    } else {
        document.exitFullscreen();
    }
}

video.addEventListener("click", togglePlay);
video.addEventListener("play", updateButton);
video.addEventListener("pause", updateButton);
video.addEventListener("timeupdate", onTimeUpdate);
toggle.addEventListener("click", togglePlay);

skipButtons.forEach(button => button.addEventListener("click", () => skip(parseInt(button.dataset.skip))));
ranges.forEach(range => range.addEventListener("change", onRangeChange));

let isClicking = false;
progress.addEventListener("mousedown", () => isClicking = true);
progress.addEventListener("mouseup", () => isClicking = false);
progress.addEventListener("mousemove", (e) => isClicking && handleSeeking(e));
progress.addEventListener("click", handleSeeking);

fullscreenButton.addEventListener("click", handleFullscreen);