let countdown;
const timeLeftDisplay = document.querySelector(".display__time-left");
const endTimeDisplay = document.querySelector(".display__end-time");

function timer(seconds) {
    clearInterval(countdown);
    const now = Date.now();
    const then = now + seconds * 1000;

    countdown = setInterval(() => {
        const secondsLeft = Math.round((then - Date.now()) / 1000);
        if (secondsLeft < 0) {
            clearInterval(countdown);
            return;
        }
        displayTimeLeft(secondsLeft);
    }, 1000);
    displayTimeLeft(seconds);
    displayEndTime(then);
}

function displayTimeLeft(secondsLeft) {
    const minutes = Math.floor(secondsLeft / 60);
    const seconds = secondsLeft % 60;
    timeLeftDisplay.textContent = `${("0" + minutes).slice(-2)}:${("0" + seconds).slice(-2)}`;
}

function displayEndTime(timestamp) {
    const then = new Date(timestamp);
    const hours = then.getHours();
    const minutes = then.getMinutes();
    const seconds = then.getSeconds();
    endTimeDisplay.textContent = `Be back at: ${("0" + hours).slice(-2)}:${("0" + minutes).slice(-2)}:${("0" + seconds).slice(-2)}`;
}

function onClick() {
    timer(parseInt(this.dataset.time));
}

function onSubmit(e) {
    e.preventDefault();
    timer(parseInt(this.minutes.value) * 60);
    this.reset();
}

const buttons = document.querySelectorAll(".timer__button");
buttons.forEach(button => button.addEventListener("click", onClick));

document.customForm.addEventListener("submit", onSubmit);