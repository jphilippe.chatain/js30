# What did I learn?
I was expecting the "capture" argument to be about stopping the bubbling up of the event, instead it is about doing a capture down where the parents process the event before the children.
To avoid the event being processed by the different elements the call is e.stopPropagation().
The "once" argument is also neat to avoid a user to click several times on a button that should only be clicked once, like a "checkout" button.