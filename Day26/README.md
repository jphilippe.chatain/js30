# What did I learn?
To get a child element when processing an event, `this.querySelector(".dropdown");`. It allows you to only query down the tree, not outside of your current "scope". 